$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});
// $(document).ready(function () {
//   var val = $(".viewContainer .activate-category.active").attr("data-category");
//   //document.cookie="activetab="+val;
//   $.get("/set-cookie?name=activetab&&value=" + val);
// });

function isDoubleClicked(element) {
  //if already clicked return TRUE to indicate this click is not allowed
  if (element.data("isclicked")) return true;

  //mark as clicked for 1 second
  element.data("isclicked", true);
  setTimeout(function () {
    element.removeData("isclicked");
  }, 5000);

  //return FALSE to indicate this click was allowed
  return false;
}

function sendPhoneNum() {
	$('#close').on('click',function () {
		$('#codeVerification').val('');
		$("#codeVerification").removeClass("is-invalid");
		$(".for_error").addClass("d-none");
	})
  var phone = $("#phoneNumberAuth").val();
  $("#authFirstStep").attr("disabled", "true");
  if (isDoubleClicked($(this))) return;
  // var keyCode = e.keyCode || e.which;
  // if (keyCode === 13) {
  //   e.preventDefault();
  //   return false;
  // }
  $.post("/"+language_locale+"/signin", { phone: phone }, function (response) {
    switch(response.status) {
      case 'ok':
        $("#verificate").modal({ show: true });

        break;
      case 'block':
        Swal.fire({
          title: 'OziqOvqat',
          text: 'Сизнинг аккаунтингиз администратор томондан блокланган',
          type: 'error',
        })
     //   $('.modal').modal('toggle');
        $("#authorization .close").click()
       // $('#authorization').fadeOut();
        $('#phoneNumberAuth').val(' ');
        $('#codeSelect').val(' ');
        //$('.modal-backdrop.fade.show').remove();
        //$('body').removeClass('modal-open')
        break;
        case 'spam':
        Swal.fire({
          title: 'OziqOvqat',
          text: 'Сиз коп маротаба уриниш қилдинкиз бир оздан сўнг уриниб кўринг',
          type: 'error',
        })
     //   $('.modal').modal('toggle');
        $("#authorization .close").click()
       // $('#authorization').fadeOut();
        $('#phoneNumberAuth').val(' ');
        $('#codeSelect').val(' ');
        //$('.modal-backdrop.fade.show').remove();
        //$('body').removeClass('modal-open')
        break;
      default:
        $("#authFirstStep").removeAttr("disabled", "true");
    }


  });
}

function verificateCode() {
  var code = $("#codeVerification").val();
  var phone = $("#phoneNumberAuth").val();
  $.post(
    "/"+language_locale+"/verification",
    {
      phone: phone,
      code: code,
    },
    function (response) {
      if (response.status == "ok") {
        // $("#category").modal({show:true});
        window.location.replace('/')
      }
      else {
        $("#codeVerification").addClass("is-invalid");
        $(".for_error")
          .html( '<div class="invalid-feedback " style="display:block">' + response.message + "</div>" );
      }
    }
  );
}
$("#authFirstStep").click(function () {
  $("#auth_form_phone").submit();
});
$("#userVerifyBtn").on("click", function () {
  $("#auth_form_verificate_code").submit();
});
$("#auth_form_phone").on("submit", function (e) {
  e.preventDefault();
  sendPhoneNum();
});

$("#auth_form_verificate_code").on("submit", function (e) {
  e.preventDefault();
  verificateCode();
});
$("#set_user_section").on("click", function () {
  var id = $("#section-items").find(".active").find("img").attr("data-pk");
  if (id) {
    $.post("/"+language_locale+"/set-section", { id: id }, function (response) {
      window.location.reload();
    });
  }
});

$(document).on("click", ".header-left .nice-select .list li", function () {
  let region = $(this).attr("data-value");
  $.post("/"+language_locale+"/set-region", { region: region }, function (response) {
    // if (response.status == "ok") {
      window.location.reload();
    // }
  });
});

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
  },
});

const $date = $(".docs-date");
const $container = $(".docs-datepicker-container");
const $trigger = $(".docs-datepicker-trigger");
const startDate = $date.attr("data-start");
const endDate = $date.attr("data-end");
const options = {
  format: "yyyy-mm-dd",
  startDate: startDate,
  endDate: endDate,
  show: function (e) {
    // console.log(e.type, e.namespace);
  },
  hide: function (e) {
    //console.log(e.type, e.namespace);
  },
  pick: function (e) {
    $.post(
      "/"+language_locale+"/pick-time",
      {
        time: e.date.getTime(),
        section_id: $("#datapicer").attr("data-section"),
      },
      function (response) {
        if (!response.times) {

          Swal.fire({
              title: 'OziqOvqat',
              text: 'Бу кунда бўш вақтлар қолмади'
            //  type: 'success',
            })
          $('#datapicer').val(' ')
        }
        if (response.status == "ok" && response.times) {
          $(document).click();
          $("#timeSelect").html(" ")

          $.each(response.times, function (index, value) {
            $("#timeSelect").append(
              "<option value='" + value + "'>" + value + "</option>"
            );
          });
        } else {
          $(document).click();
        }
        $("#timeSelect").niceSelect("update");
      }
    );
  },
  beforeShowDay: function(date){
    dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
    if(disableDates.indexOf(dmy) != -1){
      return false;
    }
    else{
      return true;
    }
  }
};

$(document).on("click", ".catalog-item .hear_icon", function () {
  var ads = $(this).attr("data-pk");
  $.post("/"+language_locale+"/add-favorite", { ads: ads }, function (response) {});
  $(this).children(".fa-heart").toggleClass("icons");
  $(this).toggleClass("active");
});
