// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.scroll_height').outerHeight();

$(window).scroll(function(event){
  didScroll = true;
});

setInterval(function() {
  if (didScroll) {
    hasScrolled();
    didScroll = false;
  }
}, 250);

function hasScrolled() {
  var st = $(this).scrollTop();

  if(Math.abs(lastScrollTop - st) <= delta)
    return;

  if (st > lastScrollTop && st > navbarHeight){
    // Scroll Down
    $('.mobile_footer').removeClass('scroll-up');

  } else {
    // Scroll Up
    if(st + $(window).height() < $(document).height()) {
      $('.mobile_footer').addClass('scroll-up');
    }
  }

  lastScrollTop = st;
}


$('.mobile-logo').click(function() {
  $(".mobile_block").addClass('active_block');
});


$('.block_none_btn').click(function() {
  $(".mobile_block").removeClass('active_block');
});


$('.search_mobile').click(function() {
  $(".search_mobile_item").addClass('active_search');
});


$('.search_none_btn').click(function() {
  $(".search_mobile_item").removeClass('active_search');
});



$(window).scroll(function(){
  if ($(this).scrollTop() > 197) {
    $('.catalog_categories').addClass('fixed');
  } else {
    $('.catalog_categories').removeClass('fixed');
  }
});

$(window).scroll(function(){
  if ($(this).scrollTop() > 197) {
    $('.catalog_p').addClass('fixed_p');
  } else {
    $('.catalog_p').removeClass('fixed_p');
  }
});
